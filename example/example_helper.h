// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_EXAMPLE_HELPER_H
#define CPP20PRINT_EXAMPLE_HELPER_H

#include <iostream>

namespace cpp20print::example {

struct NotPrintable {};

class HasStringMethod {
    public:
        std::string string() const;
};

class HasCustomOstreamOperator {
    public:
        std::string string() const;

    friend std::ostream& operator<<(
            std::ostream&, 
            const HasCustomOstreamOperator&
            );
};

}

#endif
