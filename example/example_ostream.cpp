// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <string>
#include <unordered_map>
#include <vector>

#include "cpp20-print/print.h"
#include "cpp20-print/print_options.h"
#include "cpp20-print/print_shortcuts.h"
#include "cpp20-print/print_ostream.h"

#include "example_helper.h"

// NOTE: This example tests some experimental features provided by the
// "print_ostream.h" header.
// For examples using the print function directly see the example.cpp file.

int main(int, char**) {
    using namespace cpp20print;
    using printing::print;

    std::cout 
        << "**************************************************************" 
        << "\n";
    std::cout 
        << "* Demonstration of print() features using ostream operator<< *" 
        << "\n";
    std::cout 
        << "**************************************************************" 
        << "\n\n";

    // NOTE: The following will use the default ostream operators.

    // simple print
    std::cout << "Hello world!\n";

    // printing strings
    std::string string01 {"This is a string."};
    std::string string02 {"This is another string."};

    std::cout << "Print several strings: " << string01 << "; " 
        << string02 << "\n";

    // Using the ostream operator<< defined in print_ostream.h we can now 
    // print lists to ostreams, e.g., to std::cout.
    // The operator has to be imported to the namespace.

    using ostreamoperator::operator<<;

    // NOTE: using the ostream operator we can't use print options.
   
    // print lists -- vectors

    std::vector vector01 {1, 2, 3, 4};
    std::vector vector02 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::cout << "A short list: "  << vector01 << "\n";
    std::cout << "A longer list: " << vector02 << "\n";

    // Strings and numbers will still be printed normally via the default 
    // operator.
    
    std::cout << "This uses the normal operator<<: " << string01
        << ", " << 1 << ", " << 3.1415 << "\n";

    // We haven't explicitly defined a function for printing a std::set.
    // But due to the power of templates with concepts it is recognized as a
    // list-like object, since we can iterate over it.

    std::set<int> set01 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::cout << "Printing a set using the power of templates with concepts: "
        << set01 << "\n";

    // print lists -- maps

    std::map<int, std::string> map01 {
        {1, "A"}, {2, "B"}, {3, "C"}
    };

    std::cout << "A map: " << map01 << "\n";

    std::map<int, std::string> map02 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    std::cout << "A map with more items: " <<  map02 << "\n";

    std::unordered_map<int, std::string> map03 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    std::cout << "An unordered map with more items: " << map03 << "\n";

    // print arbitrary objects using a fallback

    example::NotPrintable notPrintable {};
    std::cout << "Not printable objects are replaced: " << notPrintable 
        << "\n";

    // print objects with a string() method
    
    example::HasStringMethod hasStringMethod;
    std::cout << "A class with a string() method: " << hasStringMethod 
        << "\n";

    // print objects with a custom ostream operator<<
    
    example::HasCustomOstreamOperator hasCustomOstreamOperator;
    std::cout << "An ostream operator<< takes precedence: " << 
        hasCustomOstreamOperator << "\n";

}

