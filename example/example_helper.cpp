// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#include "example_helper.h"

namespace cpp20print::example {

std::string HasStringMethod::string() const {
    std::string s {"This is the output of the string() method."};
    return s;
}

std::string HasCustomOstreamOperator::string() const {
            std::string s {"This is the output of the string() method."};
            return s;
        }

std::ostream& operator<<(std::ostream& out, const HasCustomOstreamOperator&) {
    out << "This is the output of the custom ostream operator<<.";
    return out;
}

}

