// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#include <fstream>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <string>
#include <unordered_map>
#include <vector>

#include "cpp20-print/print.h"
#include "cpp20-print/print_options.h"
#include "cpp20-print/print_shortcuts.h"

#include "example_helper.h"

int main(int, char**) {
    using namespace cpp20print;
    using printing::print;

    print("*************************************");
    print("* Demonstration of print() features *");
    print("*************************************");
    print("");

    // simple print
    print("Hello world!");

    // printing strings
    std::string string01 {"This is a string."};
    std::string string02 {"This is another string."};

    print("Print several strings:", string01, string02);

    // print options
    namespace prt = printing::shortcuts;

    // Now we can, e.g., use prt::Green instead of 
    // printing::PrintOptions::Color::Green
    
    print("This is green text.", prt::Options {.color = prt::Green});
    print("This is yellow text.", prt::Options {.color = prt::Yellow});
    print("This is bold cyan text.", prt::Options {
            .color = prt::Cyan,
            .weight = prt::Bold
            });

    print(  "Print several strings, without spaces: ", 
            string01, 
            string02, 
            prt::Options {.seperator = ""}
            );

    print("Continue on same line:", prt::Options {.end = " "});
    print(string01, prt::Options {.end = " "});
    print(string02);

    // print lists -- vectors

    std::vector vector01 {1, 2, 3, 4};
    std::vector vector02 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    print("A short list:", vector01);
    print("A longer list:", vector02);
    print("A longer list with more elements shown:", vector02, prt::Options{
            .listStyle = {.maxElementsPrinted = 7}
            });
    print("A longer list with all elements shown:", vector02, prt::Options{
            .listStyle = {.expand = true}
            });

    // We haven't explicitly defined a function for printing a std::set.
    // But due to the power of templates with concepts it is recognized as a
    // list-like object, since we can iterate over it.
    //
    // NOTE: Some containers like std::stack or std::queue are not iterable by
    // design. Therefore we can't print their elements. This is a limitation of
    // the class not the print function.
    
    std::set<int> set01 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::deque<int> queue01 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::queue<int> queue02 {queue01};

    print("Printing a set using the power of templates with concepts:", set01);
    print("It's the same for printing a std::deque:", queue01);
    print("We can't print a std::queue since it is not iterable:", queue02);

    // print lists -- maps

    std::map<int, std::string> map01 {
        {1, "A"}, {2, "B"}, {3, "C"}
    };

    print("A map:", map01);

    std::map<int, std::string> map02 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    print("A map with more items:", map02);

    std::unordered_map<int, std::string> map03 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    print("An unordered map with more items:", map03);

    // print lists with different style

    prt::ListStyle dictionaryStyle {
        .delimiters = {"{", "}"},
        .seperator = "; "
    };
    prt::ListStyle dictionaryItemStyle {
            .delimiters = {"",""},
            .seperator = {" : "}
            };

    print("A map with a different style:", map01, prt::Options{
            .listStyle = dictionaryStyle,
            .pairStyle = dictionaryItemStyle
            });

    // print numbers

    print("Decimal numbers:",       1024, 0xff, 020);
    print("Hexadecimal numbers:",   1024, 0xff, 020, prt::Options {
            .integerFormat = prt::Hexadecimal
            });
    print("Octal numbers:",         1024, 0xff, 020, prt::Options {
            .integerFormat = prt::Octal
            });

    print("Floating point numbers:", 1.0, 3141.5, 0.01);
    print(  "Floating point numbers, scientific notation:", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Scientific}
            );
    print(  "Floating point numbers, fixed width notation:", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Fixed}
            );
    print(  "Floating point numbers, scientific notation (higher precision):", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Scientific, .floatPrecision = 5}
            );
    print(  "Floating point numbers, fixed width notation (higher precision):", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Fixed, .floatPrecision = 5}
            );

    // print arbitrary objects using a fallback

    example::NotPrintable notPrintable {};
    print("Not printable objects are replaced:", notPrintable);

    // print objects with a string() method
    
    example::HasStringMethod hasStringMethod;
    print("A class with a string() method:", hasStringMethod);

    // print objects with a custom ostream operator<<
    
    example::HasCustomOstreamOperator hasCustomOstreamOperator;
    print("An ostream operator<< takes precedence:", hasCustomOstreamOperator);

    // print to std::cerr

    print("This is an error message.", prt::Options {
            .file = std::cerr,
            .color = prt::Error
            });

    // print to file
    std::filesystem::path filePath {"test.txt"};
    std::ofstream testFile {filePath};
    print("This is printed to a file.", prt::Options {.file = testFile});
    print("Printed output to file:", filePath);
}

