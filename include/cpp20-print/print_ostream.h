// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

/// @file
/// defines an ostream operator that will use the print function to print 
/// anything to an ostream. 
/// NOTE: This is a somewhat experimental feature.

#ifndef CPP20PRINT_PRINT_OSTREAM_H
#define CPP20PRINT_PRINT_OSTREAM_H

#include "print.h"
#include "print_options.h"
#include "print_concepts.h"

namespace cpp20print::ostreamoperator {

template<class T> requires (concepts::DefaultPrintable<T> == false)
std::ostream& operator<<(std::ostream& out, const T& t) {
    printing::print(t, printing::PrintOptions {.end = "", .file = out});
    return out;
}
    
}

#endif
