// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_PRINT_LISTS_H
#define CPP20PRINT_PRINT_LISTS_H


/// @file
/// Defines functions that print lists.

#include "print_options.h"
#include "print_concepts.h"

namespace cpp20print::printing {

/// A list that is not a string or unicode string.
template<class T> 
concept PrintableListNoString = 
        concepts::PrintableList<T> &&
        concepts::Printable<T> ==  false &&
        concepts::UnicodeString<T> == false;

}

namespace cpp20print::printing::detail {

// forward declarations 

template<class T>
void print_element(std::ostream&, const PrintOptions&, const T&);

template<concepts::PrintablePair T>
void print_element(std::ostream&, const PrintOptions&, const T&);

template<PrintableListNoString T>
void print_element(std::ostream&, const PrintOptions&, const T&);

/// Prints a pair.
template<concepts::PrintablePair P>
void print_pair (
        std::ostream& out, 
        const P& pair,
        const PrintOptions& options
        ) 
{
    out << options.pairStyle.delimiters.first;
    print_element(out, options, pair.first);
    out << options.pairStyle.seperator;
    print_element(out, options, pair.second);
    out << options.pairStyle.delimiters.second;
}

/// Prints an ordered list. (Will be truncated as [1, 2, 3, ..., 100]).
/// Individual elements will be printed as if directly passed to print.
template<concepts::PrintableList L> requires concepts::HasLastElement<L>
void print_list(
        std::ostream& out, 
        const L& list, 
        const PrintOptions& options = {}
        )
{
    const auto& listStyle = options.listStyle;
    out << listStyle.delimiters.first;

    auto childOptions {options};
    if (options.listStyle.subListStyle != nullptr) {
        childOptions.listStyle = *options.listStyle.subListStyle;
    }

    const auto& end = *(--list.end());
    size_t count {0};
    for (const auto& element: list) {
        ++count;
        print_element(out, options, element);

        if (&element != &end) {
            out << listStyle.seperator;
            if (    count == listStyle.maxElementsPrinted - 1 && 
                    !listStyle.expand
                    ) 
            {
                if (list.size() > listStyle.maxElementsPrinted) 
                    out << "..." << listStyle.seperator;
                print_element(out, childOptions, end);
                break;
            }
        }
        
    }

    out << listStyle.delimiters.second;
    
}

/// Prints an unordered list. (Will be truncated as [1, 2, 3, ...]).
/// Individual elements will be printed as if directly passed to print.
template<concepts::PrintableList L>
void print_list(
        std::ostream& out, 
        const L& list, 
        const PrintOptions& options = {}
        )
{
    const auto& listStyle = options.listStyle;
    out << listStyle.delimiters.first;

    auto childOptions {options};
    if (options.listStyle.subListStyle != nullptr) {
        childOptions.listStyle = *options.listStyle.subListStyle;
    }

    size_t count {0};
    for (const auto& element: list) {
        ++count;
        print_element(out, childOptions, element);

        if (count != list.size()) {
            out << listStyle.seperator;
            if (    count == listStyle.maxElementsPrinted &&
                    !listStyle.expand
                    ) 
            {
                    out << "...";
                break;
            }
        }
        
    }

    out << listStyle.delimiters.second;
}

}

#endif
