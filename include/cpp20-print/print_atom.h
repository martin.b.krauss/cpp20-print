// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_PRINT_ATOM_H
#define CPP20PRINT_PRINT_ATOM_H

/// @file
/// Defines functions for printing individual objects (as opposed to containers
/// like lists).
/// The appropriate print function will be statically resolved using templates
/// with concepts.

#include "print_options.h"
#include "print_concepts.h"

namespace cpp20print::printing::detail {

/// Prints single object. Fallback for all other objects.
template <class T> 
inline void print_atom(std::ostream& out, const PrintOptions&, const T&) {
    //// std::addressof(x) won't use overloaded reference operator 
    //// unlike &x
    //auto address = reinterpret_cast<const int64_t>(std::addressof(t));
    //std::stringstream buffer;
    //buffer << std::hex << address;
    //std::string string = "<NOT_PRINTABLE [0x" + buffer.str() + "]>";
    std::string string = "<NOT_PRINTABLE>";
    out << string;
}

/// Prints single object. Use default operator<< if defined.
template <concepts::Printable T> requires 
    (concepts::PrintableInteger<T> == false) &&
    (concepts::PrintableFloat<T> == false)
inline void print_atom(std::ostream& out, const PrintOptions&, const T& t) {
    out << t; 
}

/// Prints single object. Uses string() method if T has one.
/// If operator<< is defined it takes precedence.
template <class T> requires 
    concepts::HasStringMethod<T> &&
    (concepts::Printable<T> == false)
inline void print_atom(std::ostream& out, const PrintOptions&, const T& t) {
    out << t.string(); 
}

/// Prints single element. For unicode strings.
template <concepts::UnicodeString T> 
inline void print_atom(std::ostream& out, const PrintOptions&, const T& t) {
    out << t; 
}

/// Prints single element. Overload to handle int options
template <concepts::PrintableInteger T>
inline void print_atom(
        std::ostream& out, 
        const PrintOptions& options, 
        const T& t
        ) 
{
    std::stringstream buffer;

    switch (options.integerFormat) {
        case PrintOptions::IntegerFormat::Decimal:
            buffer << std::dec;
            break;
        case PrintOptions::IntegerFormat::Hexadecimal:
            buffer << "0x" << std::hex;
            break;
        case PrintOptions::IntegerFormat::Octal:
            buffer << "0o" << std::oct;
            break;
    }

    buffer << static_cast<long long int>(t); 
    out << buffer.str();
}

/// Prints single element. Overload to handle int options
template <concepts::PrintableFloat T>
inline void print_atom(
        std::ostream& out, 
        const PrintOptions& options, 
        const T& t
        ) 
{
    std::stringstream buffer;

    switch (options.floatFormat) {
        case PrintOptions::FloatFormat::Default:
            break;
            case PrintOptions::FloatFormat::Scientific:
            buffer.precision(options.floatPrecision);
            buffer << std::scientific;
            break;
        case PrintOptions::FloatFormat::Fixed:
            buffer.precision(options.floatPrecision);
            buffer << std::fixed;
            break;
    }

    buffer << t; 
    out << buffer.str();
}

}

#endif
