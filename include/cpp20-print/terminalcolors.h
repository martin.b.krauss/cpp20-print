// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_TERMINALCOLORS_H
#define CPP20PRINT_TERMINALCOLORS_H

/// @file
/// Contains constants and helper classes to enable easy formatting of terminal
/// output.

#include <string>
#include <iostream>

namespace cpp20print::terminal {

/// Defines values to select colors for printing to a terminal.
/// Provides aliases for error (red), warning (yellow), etc.
enum class Color {
    System,
    Red,
    Green,
    Blue,
    Yellow,
    Magenta,
    Cyan,
    White,
    Error = Red,
    Warning = Yellow,
    Fail = Magenta,
    Notification = Blue,
    Emphasis = Cyan,
    Success = Green,
    Default = System    
};

/// Defines values to select a font weight for printing to a terminal.
enum class Weight {
    Normal,
    Bold,
    Default = Normal
};

/// Defines constants for terminal escape sequences, which can be used to
/// format color and weight of terminal fonts.
struct TerminalColor {
    const static std::string Reset       ;
    const static std::string Black       ;
    const static std::string Red         ;
    const static std::string Green       ;
    const static std::string Yellow      ;
    const static std::string Blue        ;
    const static std::string Magenta     ;
    const static std::string Cyan        ;
    const static std::string White       ;
    const static std::string BoldBlack   ;
    const static std::string BoldRed     ;
    const static std::string BoldGreen   ;
    const static std::string BoldYellow  ;
    const static std::string BoldBlue    ;
    const static std::string BoldMagenta ;
    const static std::string BoldCyan    ;
    const static std::string BoldWhite   ;

    /// Get an escape sequence encoding a color and weight for formatting
    /// terminal output.
    ///
    /// @param color specifies font color
    /// @param weight specifies font weight
    ///
    /// @return Escape sequence
    static std::string get(const Color, const Weight = Weight::Default);
};

/// Helper class that keeps track of stream flags.
///
/// When a StreamModifier object goes out of scope, the stream's initial flags
/// and formatting will be restored. (Using a StreamModifier object will make 
/// sure that even when an exception is thrown (and handled), the stream will 
/// be reset properly.)
class StreamModifier {
    public:
        /// Initializes class and saves current flags of the stream.
        /// 
        /// @param stream any console or file output stream.
        StreamModifier (std::ostream& stream) :
            stream_ {stream},
            flags_ {stream.flags()}
        { }

        /// Set the output color and font weight for the associated stream.
        ///
        /// @param color specifies font color
        /// @param weight specifies font weight
        void set_color(
                const Color color, 
                const Weight weight = Weight::Default
                ) 
        {
            // Use escape sequences only for terminal output (not file)
            if (&stream_ == &std::cout || &stream_ == &std::cerr) {
                stream_ << TerminalColor::get(color, weight);
            }
        }

        /// Resets the flags and output color of the stream.
        ~StreamModifier() noexcept {
            // Use escape sequences only for terminal output (not file)
            if (&stream_ == &std::cout || &stream_ == &std::cerr) {
                stream_ << TerminalColor::Reset << std::flush;
            }
            stream_.flags(flags_);
        }

    private:
        std::ostream& stream_;
        std::ios_base::fmtflags flags_;
};


}

#endif
