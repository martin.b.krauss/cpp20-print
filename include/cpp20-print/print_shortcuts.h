// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_SHORTCUTS_H
#define CPP20PRINT_SHORTCUTS_H

/// @file
/// Provides shortcuts for simpler use of the print function with options.
/// Include this header and alias the namespace, e.g.,
///     `namespace prt = cpp20print::printing::shortcuts;`
/// Then use as `print("text", prt::Options {.color = prt::Green});`.

#include "print_options.h"
#include "print.h"

namespace cpp20print::printing::shortcuts {

using printing::print;

using Color = printing::PrintOptions::Color;
using Weight = printing::PrintOptions::Weight;
using Options = printing::PrintOptions;

[[maybe_unused]] constexpr Color Red             {Color::Red};
[[maybe_unused]] constexpr Color Green           {Color::Green};
[[maybe_unused]] constexpr Color Blue            {Color::Blue};
[[maybe_unused]] constexpr Color Yellow          {Color::Yellow};
[[maybe_unused]] constexpr Color Magenta         {Color::Magenta};
[[maybe_unused]] constexpr Color Cyan            {Color::Cyan};
[[maybe_unused]] constexpr Color White           {Color::White};
[[maybe_unused]] constexpr Color Error           {Color::Error};
[[maybe_unused]] constexpr Color Fail            {Color::Fail};
[[maybe_unused]] constexpr Color Notification    {Color::Notification};
[[maybe_unused]] constexpr Color Emphasis        {Color::Emphasis};
[[maybe_unused]] constexpr Color Success         {Color::Success};

[[maybe_unused]] constexpr Weight Bold {Weight::Bold};

using IntegerFormat = printing::PrintOptions::IntegerFormat;
using FloatFormat = printing::PrintOptions::FloatFormat;

[[maybe_unused]] constexpr IntegerFormat Decimal    {IntegerFormat::Decimal};
[[maybe_unused]] constexpr IntegerFormat Hexadecimal  
        {IntegerFormat::Hexadecimal};
[[maybe_unused]] constexpr IntegerFormat Octal      {IntegerFormat::Octal};

[[maybe_unused]] constexpr FloatFormat Scientific  {FloatFormat::Scientific};
[[maybe_unused]] constexpr FloatFormat Fixed       {FloatFormat::Fixed};

using printing::ListStyle;

}

#endif
