// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_PRINT_OPTIONS_H
#define CPP20PRINT_PRINT_OPTIONS_H

/// @file
/// Defines structures for passing printing options.

#include <string>
#include <ostream>
#include <sstream>
#include <memory>

#include "terminalcolors.h"

namespace cpp20print::printing {

/// Holds formatting information for lists.
struct ListStyle {
    /// Defines the symbols that are printed at the beginning and end of the
    /// list.
    std::pair<std::string, std::string> delimiters {"[", "]"};

    /// Defines the symbol that seperates list items.
    std::string seperator {", "};

    /// Defines the maximum number of items before the list will be truncated.
    size_t maxElementsPrinted {5};

    /// If true lists won't be truncated.
    bool expand {false};

    /// Optionally defines the style for nested lists, if different.
    std::shared_ptr<ListStyle> subListStyle {nullptr}; 
};

/// Sets the options of the print() function.
/// Use, e.g., like `print(..., PrintOptions {.seperator=",", .end=""})`.
struct PrintOptions {
    /// Defines how the output of each argument of the print function is 
    /// seperated from the next one.
    std::string seperator {" "};

    /// Defines the terminating character in the output of print i
    /// (default="\n")
    std::string end {"\n"};

    /// Defines the stream to which the output of print is directed 
    /// (default="std::cout")
    std::reference_wrapper<std::ostream> file {std::cout};

    /// If true appends std::flush to the stream (default=false)
    bool flush {false};

    using Color = terminal::Color;
    using Weight = terminal::Weight;

    /// Defines the color of the output.
    Color color {Color::Default};

    /// Defines the font weight of the output.
    Weight  weight {Weight::Default};

    /// Defines values for formatting integers.
    enum class IntegerFormat {
        Decimal,                ///< Decimal format ("12")
        Hexadecimal,            ///< Hexadecimal format ("0xc")
        Octal,                  ///< Octal format ("0o14")
        Default = Decimal       
    };

    /// Defines the format for printing integers.
    IntegerFormat integerFormat {IntegerFormat::Default};

    /// Defines values for formatting floating point numbers (float, double).
    enum class FloatFormat {
        Default,            ///< The system default ("1024.5")
        Scientific,         ///< Scientific notation ("1.0245e3")
        Fixed               ///< Fixed notation ("1024.500")
    };

    /// Defines the format for printing floating point numbers.
    FloatFormat floatFormat {FloatFormat::Fixed};

    /// Defines how many digits are printed for floating point numbers,
    /// when scientific or fixed notation is used.
    int floatPrecision {2};

    /// Defines the formatting style for lists (std::vector, std::map, ...).
    ListStyle listStyle {};

    /// Defines the formatting style for pairs (std::pair, map items, ...).
    ListStyle pairStyle {
        .delimiters = {"(", ")"},
        .seperator  = ", "
        };

};

}

#endif
