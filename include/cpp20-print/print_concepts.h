// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_PRINT_CONCEPTS_H
#define CPP20PRINT_PRINT_CONCEPTS_H

/// @file
/// Defines concepts for different types of printable objects.
/// Requires C++20 features enabled and a recent compiler.

#include <concepts>
#include <iostream>
#include <string>
#include <filesystem>

namespace cpp20print::concepts {

/// Objects that can be printed to a std::ostream.
template<class T>
concept Printable = requires(T t) {
    {std::cout << t} -> std::same_as<std::ostream&>;
};

/// Objects that can be printed as a list.
/// Requires that begin() returns a std::forward_iterator.
/// Examples are std::vector and std::map.
template<class T> 
concept PrintableList = requires (T a) {
        {a.begin()} -> std::forward_iterator;
    };

/// Lists that have a defined last element.
/// This is, e.g., the case for std::map but not for std::unordered_map.
/// Needed for formatting truncated lists ("[1, 2, 3, ..., 100]" vs. 
/// "[17, 3, 52, 1, ...]").
template<class T> 
concept HasLastElement = requires (T a) {
        {a.end()} -> std::bidirectional_iterator;
    };


/// Objects that have a first and second member.
template<class T> 
concept PrintablePair = requires (T a) {
        {a.first};
        {a.second};
    };

/// Integers but not a char.
template<class T>
concept PrintableInteger = 
    std::integral<T> &&
    std::same_as<char, T> == false;

/// Floating point numbers.
template<class T>
concept PrintableFloat = std::floating_point<T>;

/// Objects with a string() method.
template<class T>
concept HasStringMethod = requires(T t) {
    {t.string()} -> std::same_as<std::string>;    
};

/// Unicode strings.
template<class T>
concept UnicodeString =
    std::same_as<T, std::u8string> ||
    std::same_as<T, std::u16string> ||
    std::same_as<T, std::u32string>;

/// Objects that are printable in the std namespace.
template<class T>
concept StdPrintable = requires (T t) {
    {std::operator<<(std::cout, t)} -> std::same_as<std::ostream&>;
};

/// Objects that are printable by default.
/// (Needed to avoid that strings, filepaths, etc. aren't printed as lists).
template<class T>
concept DefaultPrintable = 
        StdPrintable<T> ||
        std::same_as<T, std::filesystem::path>
        ;
    
}

#endif
