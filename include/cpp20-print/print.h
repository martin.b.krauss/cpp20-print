// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#ifndef CPP20PRINT_PRINT_H
#define CPP20PRINT_PRINT_H

/// @file
/// Defines the main print function and helper functions.

#include <sstream>

#include "print_atom.h"
#include "print_concepts.h"
#include "print_list.h"
#include "print_options.h"

namespace cpp20print::printing::detail {

/// Prints a single object. Default for objects that are not lists ("atoms").
template<class T>
inline void print_element(
        std::ostream& out,
        const PrintOptions& options,
        const T& t
        ) 
{ 
    print_atom(out, options, t);
}

/// Prints a single object that is a pair.
template<concepts::PrintablePair T>
inline void print_element(
        std::ostream& out,
        const PrintOptions& options,
        const T& t
        ) 
{ 
    print_pair(out, t, options);
}

/// Prints a single object that is a list. Lists are all iterable objects,
/// except strings.
template<PrintableListNoString T>
inline void print_element(
        std::ostream& out,
        const PrintOptions& options,
        const T& t
        ) 
{ 
    print_list(out, t, options);
}

/// Prints last element if only one left after recursion.
template <class T>
inline void print_recursive(
        std::ostream& out, 
        const PrintOptions& options,
        const T& t
        ) 
{ 
    print_element(out, options, t);
}

/// Removes PrintOptions& from args, and prints last element. 
/// (At this point print options have already been parsed and can be discarded)
template <class T>
inline void print_recursive(
        std::ostream& out, 
        const PrintOptions& options,
        const T& t,
        const PrintOptions&
        ) 
{ 
    print_recursive(out, options, t);
}


/// Prints first argument.
/// Calls itself recursively with first argument removed.
/// If only one arguement left calls overloaded function to print last 
/// element without seperator.
template <class T, class ...Args>
inline void print_recursive(
        std::ostream& out, 
        const PrintOptions& options,
        const T& t, 
        const Args& ...args
        ) 
{ 
    print_element(out, options, t);
    out << options.seperator;
    print_recursive(out, options, args...);
}

/// Overloaded function; if T& is not PrintOptions& return default options.
template<class T>
inline PrintOptions get_options(const T&) {
    return PrintOptions {};
}

/// Overloaded function; if T& is PrintOptios& returns them.
inline PrintOptions get_options(const PrintOptions& options) {
    return options;
}

}

namespace cpp20print::printing {

/// Prints any object.
/// Use like print("text1", "text2", string, object).
/// Use PrintOptions to set options, e.g.,
/// print(string1, string2, PrintOptions {.seperator=" ", .end="\n"})
template <class ...Args>
void print(const Args& ...args) {
    using terminal::TerminalColor;

    // If last argument is of class print options, set the options,
    // else leave default options. 
    // (Uses overaloaded function to check for options.)
    // Folds arguments and gets last.
    PrintOptions options = (detail::get_options(args), ...); 

    std::ostream& oStream = options.file;

    // Make sure color setter is destroyed on exception, to reset stdout
    try {
        // restores colors and stream flags on destruction 
        // (also when exception is thrown).
        // [must be named to stay in scope (!!!)]
        terminal::StreamModifier streamModifier {oStream};
        streamModifier.set_color(options.color, options.weight);

        detail::print_recursive(oStream, options, args...);

        oStream << options.end;
    } catch (...) {
        // rethrows after stream flags have been reset.
        std::rethrow_exception(std::current_exception());
    }

    if (options.flush)
        oStream << std::flush;
}
    
}

#endif
