# Introduction

[[_TOC_]]

## Overview
This project provides a print function for C++ that automatically formats vectors, maps, etc.;
similar to the print function in other programming languages (e.g., python3). 
Implemented using [C++20 concepts](https://en.cppreference.com/w/cpp/language/constraints).

This project was motivated by an interest in exploring the new cocncepts in C++20, 
as well as the desire to have a convenient print function for vectors and maps,
with nice formatting.

The print() function can be used like
~~~cpp
    std::vector vector01 {1, 2, 3, 4};
    std::vector vector02 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    print("A short list:", vector01);
    print("A longer list:", vector02);

    std::map<int, std::string> map01 {{1, "A"}, {2, "B"}, {3, "C"}};

    print("A map:", map01);
~~~
~~~
A short list: [1, 2, 3, 4]
A longer list: [1, 2, 3, 4, ..., 10]
A map: [(1, A), (2, B), (3, C)];
~~~

Various formatting options are possible, e.g.,
~~~cpp
    print("Text", 1024, 3141.5, vector01, prt::Options {
            .seperator = " / ",
            .end =  " $\n",
            .file = std::cerr,
            .flush = true,
            .color = prt::Red,
            .weight = prt::Bold,
            .integerFormat = prt::Hexadecimal,
            .floatFormat = prt::Scientific,
            .floatPrecision = 3,
            .listStyle = prt::ListStyle {
                .delimiters = {"[", "]"},
                .seperator = "; "
                }
            });
~~~
which will print 
~~~
Text / 0x400 / 3.142e+03 / [0x1; 0x2; 0x3; 0x4; 0x5] $
~~~
in bold red.

See the documentation of the `PrintOptions` class and the examples below for details.

## Compilation

The following steps can be used to compile the library and the example binaries under Ubuntu. 
For other operating systems the process should be similar.

### Prerequisites

This software uses some new features of the C++20 standard, such as [concepts](https://en.cppreference.com/w/cpp/language/constraints).
It is therefore necessary to use a very recent compiler. 

For g++ at least *version 10* is required. On Ubuntu it can be installed with
(needs 'universe')
~~~
sudo apt install g++-10
~~~
Compilation was also successfully tested with `clang++-10`.

Using a recent version of `cmake` for building the binary is recommended.

### Cloning the source

Clone the repository to anywhere on your hard drive. 
~~~
cd <PATH_TO_SOURCE>
git clone https://gitlab.com/martin.b.krauss/cpp20-print.git
~~~

### Manual compilation

A simple program using print(), such as
~~~cpp
#include "cpp20-print/print.h"

int main(int, char**) {
    using namespace cpp20print;
    using printing::print;

    print("Hello world!");
}
~~~
can be saved as `simple.cpp` in the same parent directory as the soure and be compiled with 
~~~
g++-10 -o simple simple.cpp cpp20-print/src/terminal.cpp -I cpp20-print/include -std=c++20
~~~
For compilation in other directories adjust all file paths accordingly.

Run the binary with
~~~
./simple
~~~

### Compilation using cmake

Create a build directory outside the source directory and then configure it with cmake.
~~~
cd <PATH_TO_BUILD>
mkdir build
cd build
cmake <PATH_TO_SOURCE>/cpp20-print
~~~
Since the latest compiler version (install if necessary) might not be the system's default compiler, you might need to modify the last line to
~~~
cmake <PATH_TO_SOURCE>/cpp20-print -D CMAKE_CXX_COMPILER=g++-10
~~~
or if you prefer to use clang
~~~
cmake <PATH_TO_SOURCE>/cpp20-print -D CMAKE_CXX_COMPILER=clang++-10
~~~

After cmake has configured the build it should be sufficient to simply run 
~~~
make
~~~

This will create a file `libcpp20print.a` in the build directory.
To compile a program against the library
~~~
g++-10 -o simple2 simple.cpp -lcpp20print -L <PATH_TO_BUILD> -I <PATH_TO_SOURCE>/cpp20-print/include -std=c++20
~~~

Alternatively add a `cmake` target at the end of the file `CMakeLists.txt`
~~~
add_executable(
    simple3
    <PATH_TO_SIMPLE>/simple.cpp
    )

target_link_libraries(
    simple3
    PRIVATE cpp20print
    )
~~~
and run `make`.

To build a shared library use 
~~~
cmake <PATH_TO_SOURCE>/cpp20-print -D CMAKE_CXX_COMPILER=g++-10 -D BUILD_SHARED_LIBS=ON
make
~~~
Which creates `libcpp20print.so` in the build directory.
Compile the binary against the library as above.
If the shared library is not found try running the binaries with
~~~
export LD_LIBRARY_PATH="<PATH_TO_BUILD>/build"; ./simple2
export LD_LIBRARY_PATH="<PATH_TO_BUILD>/build"; ./simple3
~~~

## Examples

### Hello world

A simple program using print()

~~~cpp
#include "cpp20-print/print.h"

int main(int, char**) {
    using namespace cpp20print;
    using printing::print;

    print("Hello world!");
}
~~~

For testing save this simple program as simple.cpp in the same parent directory as the soure and compile with 
~~~
g++-10 -o simple simple.cpp cpp20-print/src/terminal.cpp -I cpp20-print/include -std=c++20
~~~

Run the binary with
~~~
./simple
~~~

We can set print options like this
~~~cpp
    printing::PrintOptions printOptions {
        .color = printing::PrintOptions::Color::Blue
        };
    print("Hello world!", printOptions);
~~~

We can also enable some shortcuts for more convenience

~~~cpp
#include "cpp20-print/print.h"
#include "cpp20-print/print_shortcuts.h"

int main(int, char**) {
    using namespace cpp20print;
    using printing::print;

    namespace prt = printing::shortcuts;
    print("Hello world!", prt::Options {.color = prt::Blue});

}
~~~

### Vectors as lists
The print function will automatically format vectors as lists 
~~~cpp
    std::vector vector01 {1, 2, 3, 4};
    std::vector vector02 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    print("A short list:", vector01);
    print("A longer list:", vector02);
    print("A longer list with more elements shown:", vector02, prt::Options{
            .listStyle = {.maxElementsPrinted = 7}
            });
    print("A longer list with all elements shown:", vector02, prt::Options{
            .listStyle = {.expand = true}
            });
~~~

~~~
A short list: [1, 2, 3, 4]
A longer list: [1, 2, 3, 4, ..., 10]
A longer list with more elements shown: [1, 2, 3, 4, 5, 6, ..., 10]
A longer list with all elements shown: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
~~~

Since we use templates and concepts every object can be printed as a list, if it's class provides a forward iterator
~~~cpp
    std::set<int> set01 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::deque<int> queue01 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    print("Printing a set using the power of templates with concepts:", set01);
    print("It's the same for printing a std::deque:", queue01);
~~~
~~~
Printing a set using the power of templates with concepts: [1, 2, 3, 4, ..., 10]
It's the same for printing a std::deque: [1, 2, 3, 4, ..., 10]
~~~
Note that, e.g., a std::queue is not iterable and therefore we cannot print it as a list. 
This is a limitation of the class, not the print function. 
By design there is no way to access its elements without modifying the queue.  

### Maps
We can also print maps
~~~cpp
    std::map<int, std::string> map01 {
        {1, "A"}, {2, "B"}, {3, "C"}
    };

    print("A map:", map01);

    std::map<int, std::string> map02 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    print("A map with more items:", map02);

    std::unordered_map<int, std::string> map03 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    print("An unordered map with more items:", map03);
~~~
~~~
A map: [(1, A), (2, B), (3, C)]
A map with more items: [(1, A), (2, B), (3, C), (4, D), ..., (6, F)]
An unordered map with more items: [(6, F), (5, E), (4, D), (3, C), (2, B), ...]
~~~

We can also change their style
~~~cpp
    prt::ListStyle dictionaryStyle {
        .delimiters = {"{", "}"},
        .seperator = "; "
    };
    prt::ListStyle dictionaryItemStyle {
            .delimiters = {"",""},
            .seperator = {" : "}
            };

    print("A map with a different style:", map01, prt::Options{
            .listStyle = dictionaryStyle,
            .pairStyle = dictionaryItemStyle
            });
~~~
~~~
A map with a different style: {1 : A; 2 : B; 3 : C}
~~~

### Numbers
There are also options to change the formatting of integers and floating point numbers
~~~cpp
    print("Decimal numbers:",       1024, 0xff, 020);
    print("Hexadecimal numbers:",   1024, 0xff, 020, prt::Options {
            .integerFormat = prt::Hexadecimal
            });
    print("Octal numbers:",         1024, 0xff, 020, prt::Options {
            .integerFormat = prt::Octal
            });

    print("Floating point numbers:", 1.0, 3141.5, 0.01);
    print(  "Floating point numbers, scientific notation:",
            1.0, 3141.5, 0.01,
            prt::Options {.floatFormat = prt::Scientific}
            );
    print(  "Floating point numbers, fixed width notation:",
            1.0, 3141.5, 0.01,
            prt::Options {.floatFormat = prt::Fixed}
            );
    print(  "Floating point numbers, scientific notation (higher precision):",
            1.0, 3141.5, 0.01,
            prt::Options {.floatFormat = prt::Scientific, .floatPrecision = 5}
            );
    print(  "Floating point numbers, fixed width notation (higher precision):",
            1.0, 3141.5, 0.01,
            prt::Options {.floatFormat = prt::Fixed, .floatPrecision = 5}
            );

~~~
~~~
Decimal numbers: 1024 255 16
Hexadecimal numbers: 0x400 0xff 0x10
Octal numbers: 0o2000 0o377 0o20
Floating point numbers: 1.00 3141.50 0.01
Floating point numbers, scientific notation: 1.00e+00 3.14e+03 1.00e-02
Floating point numbers, fixed width notation: 1.00 3141.50 0.01
Floating point numbers, scientific notation (higher precision): 1.00000e+00 3.14150e+03 1.00000e-02
Floating point numbers, fixed width notation (higher precision): 1.00000 3141.50000 0.01000
~~~

### Other objects
For objects that have a friend ostream operator<< or a string() member function, these will be used.
If both are defined the opertor takes precedence.

If an object is not printable it will be replaced by
~~~
<NOT_PRINTABLE>
~~~

### Redirecting output
The output of print can be redirected
~~~cpp
    // print to std::cerr

    print("This is an error message.", prt::Options {
            .file = std::cerr,
            .color = prt::Error
            });

    // print to file
    std::filesystem::path filePath {"test.txt"};
    std::ofstream testFile {filePath};
    print("This is printed to a file.", prt::Options {.file = testFile});
    print("Printed output to file:", filePath);
~~~ 

This will output (colors omitted)
~~~
This is an error message.
Printed output to file: "test.txt"
~~~

### Example binaries
After compiling the source with cmake, the the binaries `example` and `example_ostream` will have been
created in the build directory.
They can be run with
~~~
./example
./example_ostream
~~~
The first program will demonstrate the use of the print() function in the terminal and output also a file `test.txt`.
Its source is
~~~cpp
#include <fstream>
#include <map>
#include <set>
#include <queue>
#include <deque>
#include <string>
#include <unordered_map>
#include <vector>

#include "cpp20-print/print.h"
#include "cpp20-print/print_options.h"
#include "cpp20-print/print_shortcuts.h"

#include "example_helper.h"

int main(int, char**) {
    using namespace cpp20print;
    using printing::print;

    print("*************************************");
    print("* Demonstration of print() features *");
    print("*************************************");
    print("");

    // simple print
    print("Hello world!");

    // printing strings
    std::string string01 {"This is a string."};
    std::string string02 {"This is another string."};

    print("Print several strings:", string01, string02);

    // print options
    namespace prt = printing::shortcuts;

    // Now we can, e.g., use prt::Green instead of 
    // printing::PrintOptions::Color::Green
    
    print("This is green text.", prt::Options {.color = prt::Green});
    print("This is yellow text.", prt::Options {.color = prt::Yellow});
    print("This is bold cyan text.", prt::Options {
            .color = prt::Cyan,
            .weight = prt::Bold
            });

    print(  "Print several strings, without spaces: ", 
            string01, 
            string02, 
            prt::Options {.seperator = ""}
            );

    print("Continue on same line:", prt::Options {.end = " "});
    print(string01, prt::Options {.end = " "});
    print(string02);

    // print lists -- vectors

    std::vector vector01 {1, 2, 3, 4};
    std::vector vector02 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    print("A short list:", vector01);
    print("A longer list:", vector02);
    print("A longer list with more elements shown:", vector02, prt::Options{
            .listStyle = {.maxElementsPrinted = 7}
            });
    print("A longer list with all elements shown:", vector02, prt::Options{
            .listStyle = {.expand = true}
            });

    // We haven't explicitly defined a function for printing a std::set.
    // But due to the power of templates with concepts it is recognized as a
    // list-like object, since we can iterate over it.
    //
    // NOTE: Some containers like std::stack or std::queue are not iterable by
    // design. Therefore we can't print their elements. This is a limitation of
    // the class not the print function.
    
    std::set<int> set01 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::deque<int> queue01 {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    std::queue<int> queue02 {queue01};

    print("Printing a set using the power of templates with concepts:", set01);
    print("It's the same for printing a std::deque:", queue01);
    print("We can't print a std::queue since it is not iterable:", queue02);

    // print lists -- maps

    std::map<int, std::string> map01 {
        {1, "A"}, {2, "B"}, {3, "C"}
    };

    print("A map:", map01);

    std::map<int, std::string> map02 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    print("A map with more items:", map02);

    std::unordered_map<int, std::string> map03 {
        {1, "A"}, {2, "B"}, {3, "C"}, {4, "D"}, {5, "E"}, {6, "F"}
    };

    print("An unordered map with more items:", map03);

    // print lists with different style

    prt::ListStyle dictionaryStyle {
        .delimiters = {"{", "}"},
        .seperator = "; "
    };
    prt::ListStyle dictionaryItemStyle {
            .delimiters = {"",""},
            .seperator = {" : "}
            };

    print("A map with a different style:", map01, prt::Options{
            .listStyle = dictionaryStyle,
            .pairStyle = dictionaryItemStyle
            });

    // print numbers

    print("Decimal numbers:",       1024, 0xff, 020);
    print("Hexadecimal numbers:",   1024, 0xff, 020, prt::Options {
            .integerFormat = prt::Hexadecimal
            });
    print("Octal numbers:",         1024, 0xff, 020, prt::Options {
            .integerFormat = prt::Octal
            });

    print("Floating point numbers:", 1.0, 3141.5, 0.01);
    print(  "Floating point numbers, scientific notation:", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Scientific}
            );
    print(  "Floating point numbers, fixed width notation:", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Fixed}
            );
    print(  "Floating point numbers, scientific notation (higher precision):", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Scientific, .floatPrecision = 5}
            );
    print(  "Floating point numbers, fixed width notation (higher precision):", 
            1.0, 3141.5, 0.01, 
            prt::Options {.floatFormat = prt::Fixed, .floatPrecision = 5}
            );

    // print arbitrary objects using a fallback

    example::NotPrintable notPrintable {};
    print("Not printable objects are replaced:", notPrintable);

    // print objects with a string() method
    
    example::HasStringMethod hasStringMethod;
    print("A class with a string() method:", hasStringMethod);

    // print objects with a custom ostream operator<<
    
    example::HasCustomOstreamOperator hasCustomOstreamOperator;
    print("An ostream operator<< takes precedence:", hasCustomOstreamOperator);

    // print to std::cerr

    print("This is an error message.", prt::Options {
            .file = std::cerr,
            .color = prt::Error
            });

    // print to file
    std::filesystem::path filePath {"test.txt"};
    std::ofstream testFile {filePath};
    print("This is printed to a file.", prt::Options {.file = testFile});
    print("Printed output to file:", filePath);
}
~~~

Its output should look similar to the following (colors omitted):
~~~
*************************************
* Demonstration of print() features *
*************************************

Hello world!
Print several strings: This is a string. This is another string.
This is green text.
This is yellow text.
This is bold cyan text.
Print several strings, without spaces: This is a string.This is another string.
Continue on same line: This is a string. This is another string.
A short list: [1, 2, 3, 4]
A longer list: [1, 2, 3, 4, ..., 10]
A longer list with more elements shown: [1, 2, 3, 4, 5, 6, ..., 10]
A longer list with all elements shown: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
Printing a set using the power of templates with concepts: [1, 2, 3, 4, ..., 10]
It's the same for printing a std::deque: [1, 2, 3, 4, ..., 10]
We can't print a std::queue since it is not iterable: <NOT_PRINTABLE>
A map: [(1, A), (2, B), (3, C)]
A map with more items: [(1, A), (2, B), (3, C), (4, D), ..., (6, F)]
An unordered map with more items: [(6, F), (5, E), (4, D), (3, C), (2, B), ...]
A map with a different style: {1 : A; 2 : B; 3 : C}
Decimal numbers: 1024 255 16
Hexadecimal numbers: 0x400 0xff 0x10
Octal numbers: 0o2000 0o377 0o20
Floating point numbers: 1.00 3141.50 0.01
Floating point numbers, scientific notation: 1.00e+00 3.14e+03 1.00e-02
Floating point numbers, fixed width notation: 1.00 3141.50 0.01
Floating point numbers, scientific notation (higher precision): 1.00000e+00 3.14150e+03 1.00000e-02
Floating point numbers, fixed width notation (higher precision): 1.00000 3141.50000 0.01000
Not printable objects are replaced: <NOT_PRINTABLE>
A class with a string() method: This is the output of the string() method.
An ostream operator<< takes precedence: This is the output of the custom ostream operator<<.
This is an error message.
Printed output to file: "test.txt"
~~~

The second example demonstrates the use of print features with an ostream operator<<.

## Using C++20 concepts

The concepts that have been introduced in C++20 provide a form a static duck typing. 
We can use their power to implement a printing function that can accept various arguments
and print them to the screen depending on their properties.
We can use this feature for instance to print objects that can be iterated over as lists.

In print_concepts.h we define several concepts, such as
~~~cpp
template<class T>
concept Printable = requires(T t) {
    {std::cout << t} -> std::same_as<std::ostream&>;
};

template<class T>
concept UnicodeString =
    std::same_as<T, std::u8string> ||
    std::same_as<T, std::u16string> ||
    std::same_as<T, std::u32string>;

template<class T> 
concept PrintableList = requires (T a) {
        {a.begin()} -> std::forward_iterator;
    };

template<class T> 
concept PrintableListNoString = 
        concepts::PrintableList<T> &&
        concepts::Printable<T> ==  false &&
        concepts::UnicodeString<T> == false;

}
~~~
which defines the concept of list like types. All we need is that the object has 
a begin() method that returns a `std::forward_iterator`. This is the case, e.g., for 
`std::vector` or `std::map`. Since strings also have forward iterators, but shouldn't 
be printed as lists of letters we define a concept `PrintableListNoString`.

The main print(...) function will call the print_element(...) function for each argument.
It is defined as follows
~~~cpp
template<class T>
inline void print_element(
        std::ostream& out,
        const PrintOptions& options,
        const T& t
        ) 
{ 
    print_atom(out, options, t);
}

template<PrintableListNoString T>
inline void print_element(
        std::ostream& out,
        const PrintOptions& options,
        const T& t
        ) 
{ 
    print_list(out, t, options);
}
~~~
The overload will be statically resolved during compilation. If `T` is list-like, e.g., a 
`std::vector` the second overload will be called. In all other cases the generic template
matches and the first function will be called.

The print_list(...) function looks like
~~~cpp
template<concepts::PrintableList L>
void print_list(
        std::ostream& out, 
        const L& list, 
        const PrintOptions& options = {}
        )
{
    const auto& listStyle = options.listStyle;
    out << listStyle.delimiters.first;

    auto childOptions {options};
    if (options.listStyle.subListStyle != nullptr) {
        childOptions.listStyle = *options.listStyle.subListStyle;
    }

    size_t count {0};
    for (const auto& element: list) {
        ++count;
        print_element(out, childOptions, element);

        if (count != list.size()) {
            out << listStyle.seperator;
            if (    count == listStyle.maxElementsPrinted &&
                    !listStyle.expand
                    ) 
            {
                    out << "...";
                break;
            }
        }
        
    }

    out << listStyle.delimiters.second;
}
~~~

With the concept
~~~cpp
template<class T> 
concept HasLastElement = requires (T a) {
        {a.end()} -> std::bidirectional_iterator;
    };
~~~
we can add an overload also to print_list 
~~~cpp
/// Prints an ordered list. (Will be truncated as [1, 2, 3, ..., 100]).
/// Individual elements will be printed as if directly passed to print.
template<concepts::PrintableList L> requires concepts::HasLastElement<L>
void print_list(
        std::ostream& out, 
        const L& list, 
        const PrintOptions& options = {}
        )
{
    const auto& listStyle = options.listStyle;
    out << listStyle.delimiters.first;

    auto childOptions {options};
    if (options.listStyle.subListStyle != nullptr) {
        childOptions.listStyle = *options.listStyle.subListStyle;
    }

    const auto& end = *(--list.end());
    size_t count {0};
    for (const auto& element: list) {
        ++count;
        print_element(out, options, element);

        if (&element != &end) {
            out << listStyle.seperator;
            if (    count == listStyle.maxElementsPrinted - 1 && 
                    !listStyle.expand
                    ) 
            {
                if (list.size() > listStyle.maxElementsPrinted) 
                    out << "..." << listStyle.seperator;
                print_element(out, childOptions, end);
                break;
            }
        }
        
    }

    out << listStyle.delimiters.second;
    
}
~~~
The latter overload will be called, e.g., for a `std::vector`, the former, e.g., for a 
`std::unordered_map`. See the examples above for their different behaviour.


See the documentation of print.h, print_concepts.h, and print_atom.h for more examples.

## Licence
Copyright (c) 2021 Martin B. Krauss.

Licensed under the MIT LICENSE.

See the file 'LICENSE' that should be distributed with this software or can
be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

