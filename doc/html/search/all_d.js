var searchData=
[
  ['scientific_46',['Scientific',['../structcpp20print_1_1printing_1_1PrintOptions.html#aa900700142fa799baa05b7aa63fe3066a21234a0e100d74037a4da2e53f3200d7',1,'cpp20print::printing::PrintOptions']]],
  ['seperator_47',['seperator',['../structcpp20print_1_1printing_1_1ListStyle.html#ad6abfeabd9d43b6bc40c801ee9f8c447',1,'cpp20print::printing::ListStyle::seperator()'],['../structcpp20print_1_1printing_1_1PrintOptions.html#ad6abfeabd9d43b6bc40c801ee9f8c447',1,'cpp20print::printing::PrintOptions::seperator()']]],
  ['set_5fcolor_48',['set_color',['../classcpp20print_1_1terminal_1_1StreamModifier.html#a4fda381c4f568351b550f67d3443592c',1,'cpp20print::terminal::StreamModifier']]],
  ['stdprintable_49',['StdPrintable',['../print__concepts_8h.html#ab6a1189eb9b1269caae54f7ab7983c93',1,'cpp20print::concepts']]],
  ['streammodifier_50',['StreamModifier',['../classcpp20print_1_1terminal_1_1StreamModifier.html',1,'StreamModifier'],['../classcpp20print_1_1terminal_1_1StreamModifier.html#ad40734bf045f1fc3da4b4e6712ef5635',1,'cpp20print::terminal::StreamModifier::StreamModifier()']]],
  ['subliststyle_51',['subListStyle',['../structcpp20print_1_1printing_1_1ListStyle.html#a8f9e813f3c8489c4aa31c5e1bb5d371d',1,'cpp20print::printing::ListStyle']]]
];
