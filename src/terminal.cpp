// Copyright (c) 2021 Martin B. Krauss.
// Licensed under the MIT LICENSE.
// See the file 'LICENSE' that should be distributed with this software or can
// be found at 'https://gitlab.com/martin.b.krauss/cpp20-print' for details.

#include "terminalcolors.h"

#include <unordered_map>

namespace cpp20print::terminal {

const std::string TerminalColor::Reset       {"\033[0m"        };
const std::string TerminalColor::Black       {"\033[0m\033[30m"       };
const std::string TerminalColor::Red         {"\033[0m\033[31m"       };
const std::string TerminalColor::Green       {"\033[0m\033[32m"       };
const std::string TerminalColor::Yellow      {"\033[0m\033[33m"       };
const std::string TerminalColor::Blue        {"\033[0m\033[34m"       };
const std::string TerminalColor::Magenta     {"\033[0m\033[35m"       };
const std::string TerminalColor::Cyan        {"\033[0m\033[36m"       };
const std::string TerminalColor::White       {"\033[0m\033[37m"       };
const std::string TerminalColor::BoldBlack   {"\033[0m\033[1m\033[30m"};
const std::string TerminalColor::BoldRed     {"\033[0m\033[1m\033[31m"};
const std::string TerminalColor::BoldGreen   {"\033[0m\033[1m\033[32m"};
const std::string TerminalColor::BoldYellow  {"\033[0m\033[1m\033[33m"};
const std::string TerminalColor::BoldBlue    {"\033[0m\033[1m\033[34m"};
const std::string TerminalColor::BoldMagenta {"\033[0m\033[1m\033[35m"};
const std::string TerminalColor::BoldCyan    {"\033[0m\033[1m\033[36m"};
const std::string TerminalColor::BoldWhite   {"\033[0m\033[1m\033[37m"};

std::unordered_map<Color, std::string> colorNormal {
    {Color::System,     TerminalColor::Reset    },
    {Color::Red,        TerminalColor::Red      },
    {Color::Green,      TerminalColor::Green    },
    {Color::Blue,       TerminalColor::Blue     },
    {Color::Yellow,     TerminalColor::Yellow   },
    {Color::Magenta,    TerminalColor::Magenta  },
    {Color::Cyan,       TerminalColor::Cyan     },
    {Color::White,      TerminalColor::White    }
};

std::unordered_map<Color, std::string> colorBold {
    {Color::System,     TerminalColor::Reset    },
    {Color::Red,        TerminalColor::BoldRed      },
    {Color::Green,      TerminalColor::BoldGreen    },
    {Color::Blue,       TerminalColor::BoldBlue     },
    {Color::Yellow,     TerminalColor::BoldYellow   },
    {Color::Magenta,    TerminalColor::BoldMagenta  },
    {Color::Cyan,       TerminalColor::BoldCyan     },
    {Color::White,      TerminalColor::BoldWhite    }
};
    
std::string TerminalColor::get(const Color color, const Weight weight) {
    if (weight == Weight::Bold)
        return colorBold.at(color);
    else
        return colorNormal.at(color);
}
    
}
